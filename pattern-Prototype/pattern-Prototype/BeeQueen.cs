﻿namespace OtusTeaching.pattern_Prototype
{
    public class BeeQueen : Bee
    {
        protected readonly bool _isLabeled;
        protected readonly bool _isFetal;

        public bool IsLabeled => _isLabeled;
        public bool IsFetal => _isFetal;

        public BeeQueen(bool isLabeled, bool isFetal, string breed) : base(breed)
        {
            _isLabeled = isLabeled;
            _isFetal = isFetal;
        }

        public bool MakeABrood()
        {
            if (_isFetal)
                return BeeActionSimulating.IsSucceeded(75);
            else
                return false;
        }

        public override Bee Copy()
        {
            return new BeeQueen(_isLabeled, _isFetal, _breed);
        }
    }
}
