﻿namespace OtusTeaching.pattern_Prototype
{
    public class DroneBee : Bee
    {
        protected readonly bool _isExpelled;

        public bool IsExpelled => _isExpelled;

        public DroneBee(bool isExpelled, string breed) : base(breed, false)
        {
            _isExpelled = isExpelled;
        }

        public bool FlyForFertilizeOtherQueen()
        {
            if(!_isExpelled)
                return BeeActionSimulating.IsSucceeded(3);
            else
                return BeeActionSimulating.IsSucceeded(1);
        }

        public override Bee Copy()
        {
            return new DroneBee(_isExpelled, _breed);
        }
    }
}
