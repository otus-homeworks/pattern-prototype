﻿namespace OtusTeaching.pattern_Prototype
{
    interface IMyCloneable<T>
    {
        T Copy();
    }
}
