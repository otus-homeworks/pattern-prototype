﻿using System;

namespace OtusTeaching.pattern_Prototype
{
    public class Bee : IMyCloneable<Bee>, ICloneable
    {
        protected readonly string _breed;
        protected readonly bool _isHasASting;

        public string Breed => _breed;
        public bool IsHasASting => _isHasASting;

        public Bee(string breed, bool isHasASting = true)
        {
            _breed = breed;
            _isHasASting = isHasASting;
        }

        public virtual Bee Copy()
        {
            return new Bee(_breed, _isHasASting);
        }

        public object Clone()
        {
            return Copy();
        }
    }
}
