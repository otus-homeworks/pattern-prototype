﻿using System;

namespace OtusTeaching.pattern_Prototype
{
    public static class BeeActionSimulating
    {
        public static bool IsSucceeded(byte probabilityOfSuccessInPercent)
        {
            if (new Random().Next(101) < probabilityOfSuccessInPercent)
                return true;
            else
                return false;
        }
    }
}
