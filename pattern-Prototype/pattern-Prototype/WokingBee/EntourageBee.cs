﻿namespace OtusTeaching.pattern_Prototype
{
    public class EntourageBee : WorkingBee
    {
        protected readonly bool _isBig;        
        public bool IsBig => _isBig;

        public EntourageBee(bool isBig, string breed) : base(breed)
        {
            _isBig = isBig;
        }

        public bool ServeQueen()
        {
            if(_isBig)
                return BeeActionSimulating.IsSucceeded(93);
            else
                return BeeActionSimulating.IsSucceeded(85);
        }

        public override Bee Copy()
        {
            return new EntourageBee(_isBig, _breed);
        }
    }
}
