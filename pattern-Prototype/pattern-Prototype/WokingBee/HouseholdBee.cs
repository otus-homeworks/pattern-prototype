﻿namespace OtusTeaching.pattern_Prototype
{
    public class HouseholdBee : WorkingBee
    {
        public HouseholdBee(string breed) : base(breed)
        {
        }

        static public bool HouseholdServe()
        {
            return BeeActionSimulating.IsSucceeded(90);
        }

        public override Bee Copy()
        {
            return new HouseholdBee(_breed);
        }
    }
}
