﻿namespace OtusTeaching.pattern_Prototype
{
    public abstract class WorkingBee : Bee
    {
        public WorkingBee(string breed) : base(breed)
        {
        }        
    }
}
