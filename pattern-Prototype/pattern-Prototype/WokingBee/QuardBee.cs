﻿namespace OtusTeaching.pattern_Prototype
{
    public class QuardBee : WorkingBee
    {
        protected readonly bool _isAggressive;
        public bool IsAggressive => _isAggressive;

        public QuardBee(bool isAggressive, string breed) : base(breed)
        {
            _isAggressive = isAggressive;
        }
        
        public bool DriveAwayTheAttacker()
        {
            if(_isAggressive)
                return BeeActionSimulating.IsSucceeded(75);
            else
                return BeeActionSimulating.IsSucceeded(65);
        }
        public override Bee Copy()
        {
            return new QuardBee(_isAggressive, _breed);
        }
    }
}
