﻿namespace OtusTeaching.pattern_Prototype
{
    public class FlightBee : WorkingBee
    {
        protected readonly bool _isFlown;
        protected readonly float _avProboscisLength;
        protected readonly int _flightPerDayNum;
        protected readonly bool _isFarFlying;

        public bool IsFlown => _isFlown;
        public float AvProboscisLength => _avProboscisLength;
        public int FlightPerDayNum => _flightPerDayNum;
        public bool IsFarFlying => _isFarFlying;

        public FlightBee(bool isFlown, float avProboscisLength, int flightPerDayNum, bool isFarFlying, string breed) : base(breed)
        {
            _isFlown = isFlown;
            _avProboscisLength = avProboscisLength;
            _flightPerDayNum = flightPerDayNum;
            _isFarFlying = isFarFlying;
        }

        public bool FlyForCollectHoney()
        {
            if(_isFlown)
                return BeeActionSimulating.IsSucceeded(50);
            else
                return BeeActionSimulating.IsSucceeded(25);
        }

        public bool FlyForCollectPollen()
        {
            if (_isFlown)
                return BeeActionSimulating.IsSucceeded(40);
            else
                return BeeActionSimulating.IsSucceeded(20);
        }

        public bool FlyForFindAnything()
        {
            if (_isFarFlying)
                return BeeActionSimulating.IsSucceeded(50);
            else
                return BeeActionSimulating.IsSucceeded(20);
        }

        public override Bee Copy()
        {
            return new FlightBee(_isFlown, _avProboscisLength, _flightPerDayNum, _isFarFlying, _breed);
        }
    }
}
