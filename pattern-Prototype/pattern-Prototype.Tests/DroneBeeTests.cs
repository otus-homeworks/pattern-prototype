﻿using Xunit;

namespace OtusTeaching.pattern_Prototype.Tests
{
    public class DroneBeeTests
    {
        [Theory]
        [InlineData(true, "Среднерусская")]
        [InlineData(false, "Бурзянская")]

        public void CopyAndCloneTest(bool isExpelled, string breed)
        {
            // Arrange
            var DroneBee = new DroneBee(isExpelled, breed);

            // Act
            var DroneBeeCopy = (DroneBee)DroneBee.Copy();
            var DroneBeeClone = (DroneBee)DroneBee.Clone();

            // Assert
            Assert.NotNull(DroneBeeCopy);
            Assert.True(IsFieldsMatch(DroneBee, DroneBeeCopy));

            Assert.NotNull(DroneBeeClone);
            Assert.True(IsFieldsMatch(DroneBee, DroneBeeClone));
        }

        static private bool IsFieldsMatch(DroneBee droneBee_1, DroneBee droneBee_2)
        {
            return (droneBee_1.Breed == droneBee_2.Breed &&
                    droneBee_1.IsHasASting == droneBee_2.IsHasASting &&
                    droneBee_1.IsExpelled == droneBee_2.IsExpelled
                    );
        }
    }
}
