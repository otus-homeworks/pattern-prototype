﻿using Xunit;

namespace OtusTeaching.pattern_Prototype.Tests
{
    public class BeeQueenTests
    {
        [Theory]
        [InlineData(true, true, "Среднерусская")]
        [InlineData(true, false, "Краинская")]
        [InlineData(false, true, "Бакфаст")]
        [InlineData(false, false, "Бурзянская")]

        public void CopyAndCloneTest(bool isLabeled, bool isFetal, string breed)
        {
            // Arrange
            var BeeQueen = new BeeQueen(isLabeled, isFetal, breed);

            // Act
            var BeeQueenCopy = (BeeQueen)BeeQueen.Copy();
            var BeeQueenClone = (BeeQueen)BeeQueen.Clone();

            // Assert
            Assert.NotNull(BeeQueenCopy);
            Assert.True(IsFieldsMatch(BeeQueen, BeeQueenCopy));

            Assert.NotNull(BeeQueenClone);
            Assert.True(IsFieldsMatch(BeeQueen, BeeQueenClone));
        }

        static private bool IsFieldsMatch(BeeQueen beeQueen_1, BeeQueen beeQueen_2)
        {
            return (beeQueen_1.Breed == beeQueen_2.Breed &&
                    beeQueen_1.IsHasASting == beeQueen_2.IsHasASting &&
                    beeQueen_1.IsLabeled == beeQueen_2.IsLabeled &&
                    beeQueen_1.IsFetal == beeQueen_2.IsFetal
                    );
        }
    }
}
