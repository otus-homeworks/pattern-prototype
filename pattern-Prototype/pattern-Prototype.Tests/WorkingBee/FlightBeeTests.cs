﻿using Xunit;

namespace OtusTeaching.pattern_Prototype.Tests
{
    public class FlightBeeTests
    {
        [Theory]
        [InlineData(true, 6.1, 100, true, "Среднерусская")]
        [InlineData(true, 6.45, 70, false, "Краинская")]
        [InlineData(false, 6.7, 80, true, "Карника")]
        [InlineData(false, 6.1, 90, false, "Бурзянская")]

        public void CopyAndCloneTest(bool isFlown, float avProboscisLength, int flightPerDayNum, bool isFarFlying, string breed)
        {
            // Arrange
            var FlightBee = new FlightBee(isFlown, avProboscisLength, flightPerDayNum, isFarFlying, breed);

            // Act
            var FlightBeeCopy = (FlightBee)FlightBee.Copy();
            var FlightBeeClone = (FlightBee)FlightBee.Clone();

            // Assert
            Assert.NotNull(FlightBeeCopy);
            Assert.True(IsFieldsMatch(FlightBee, FlightBeeCopy));

            Assert.NotNull(FlightBeeClone);
            Assert.True(IsFieldsMatch(FlightBee, FlightBeeClone));
        }

        static private bool IsFieldsMatch(FlightBee flightBee_1, FlightBee flightBee_2)
        {
            return (flightBee_1.Breed == flightBee_2.Breed &&
                flightBee_1.IsHasASting == flightBee_2.IsHasASting &&
                flightBee_1.IsFlown == flightBee_2.IsFlown &&
                flightBee_1.AvProboscisLength == flightBee_2.AvProboscisLength &&
                flightBee_1.FlightPerDayNum == flightBee_2.FlightPerDayNum &&
                flightBee_1.IsFarFlying == flightBee_2.IsFarFlying
                );
        }
    }
}
