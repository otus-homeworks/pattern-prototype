﻿using Xunit;

namespace OtusTeaching.pattern_Prototype.Tests
{
    public class QuardBeeTests
    {
        [Theory]
        [InlineData(true, "Среднерусская")]
        [InlineData(false, "Бакфаст")]

        public void CopyAndCloneTest(bool isAggressive, string breed)
        {
            // Arrange
            var QuardBee = new QuardBee(isAggressive, breed);

            // Act
            var QuardBeeCopy = (QuardBee)QuardBee.Copy();
            var QuardBeeClone = (QuardBee)QuardBee.Clone();

            // Assert
            Assert.NotNull(QuardBeeCopy);
            Assert.True(IsFieldsMatch(QuardBee, QuardBeeCopy));

            Assert.NotNull(QuardBeeClone);
            Assert.True(IsFieldsMatch(QuardBee, QuardBeeClone));
        }

        static private bool IsFieldsMatch(QuardBee quardBee_1, QuardBee quardBee_2)
        {
            return (quardBee_1.Breed == quardBee_2.Breed &&
                    quardBee_1.IsHasASting == quardBee_2.IsHasASting &&
                    quardBee_1.IsAggressive == quardBee_2.IsAggressive
                    );
        }
    }
}
