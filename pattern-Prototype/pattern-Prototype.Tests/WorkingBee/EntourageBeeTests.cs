﻿using Xunit;

namespace OtusTeaching.pattern_Prototype.Tests
{
    public class EntourageBeeTests
    {
        [Theory]
        [InlineData(true, "Среднерусская")]
        [InlineData(false, "Бакфаст")]

        public void CopyAndCloneTest(bool isBig, string breed)
        {
            // Arrange
            var EntourageBee = new EntourageBee(isBig, breed);

            // Act
            var EntourageBeeCopy = (EntourageBee)EntourageBee.Copy();
            var EntourageBeeClone = (EntourageBee)EntourageBee.Clone();

            // Assert
            Assert.NotNull(EntourageBeeCopy);
            Assert.True(IsFieldsMatch(EntourageBee, EntourageBeeCopy));

            Assert.NotNull(EntourageBeeClone);
            Assert.True(IsFieldsMatch(EntourageBee, EntourageBeeClone));
        }

        static private bool IsFieldsMatch(EntourageBee entourageBee_1, EntourageBee entourageBee_2)
        {
            return (entourageBee_1.Breed == entourageBee_2.Breed &&
                    entourageBee_1.IsHasASting == entourageBee_2.IsHasASting &&
                    entourageBee_1.IsBig == entourageBee_2.IsBig
                    );
        }
    }
}
