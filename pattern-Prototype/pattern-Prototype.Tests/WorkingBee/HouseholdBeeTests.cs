﻿using Xunit;

namespace OtusTeaching.pattern_Prototype.Tests
{
    public class HouseholdBeeTests
    {
        [Fact]
        public void CopyAndCloneTest()
        {
            // Arrange
            var HouseholdBee = new HouseholdBee("Карника");

            // Act
            var HouseholdBeeCopy = (HouseholdBee)HouseholdBee.Copy();
            var HouseholdBeeClone = (HouseholdBee)HouseholdBee.Clone();

            // Assert
            Assert.NotNull(HouseholdBeeCopy);
            Assert.True(IsFieldsMatch(HouseholdBee, HouseholdBeeCopy));

            Assert.NotNull(HouseholdBeeClone);
            Assert.True(IsFieldsMatch(HouseholdBee, HouseholdBeeClone));
        }

        static private bool IsFieldsMatch(HouseholdBee beeQueen_1, HouseholdBee beeQueen_2)
        {
            return (beeQueen_1.Breed == beeQueen_2.Breed &&
                    beeQueen_1.IsHasASting == beeQueen_2.IsHasASting
                    );
        }
    }
}
