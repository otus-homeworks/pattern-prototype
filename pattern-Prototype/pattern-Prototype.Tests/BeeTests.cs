﻿using Xunit;

namespace OtusTeaching.pattern_Prototype.Tests
{
    public class BeeTests
    {
        [Theory]
        [InlineData("Бакфаст", true)]
        [InlineData("Итальянская", false)]

        public void CopyAndCloneForCreatedWithTwoParamsTest(string breed, bool isHasASting)
        {
            // Arrange
            var Bee = new Bee(breed, isHasASting);

            // Act
            var BeeCopy = Bee.Copy();
            var BeeClone = (Bee)Bee.Clone();

            // Assert
            Assert.NotNull(BeeCopy);
            Assert.True(IsFieldsMatch(Bee, BeeCopy));

            Assert.NotNull(BeeClone);
            Assert.True(IsFieldsMatch(Bee, BeeClone));
        }

        [Fact]
        public void CopyAndCloneForCreatedWithOneParamTest()
        {
            // Arrange
            var Bee = new Bee("Карпатская");

            // Act
            var BeeCopy = Bee.Copy();

            // Assert
            Assert.NotNull(BeeCopy);
            Assert.Equal(Bee.Breed, BeeCopy.Breed);
            Assert.Equal(Bee.IsHasASting, BeeCopy.IsHasASting);
        }

        static private bool IsFieldsMatch(Bee bee1, Bee bee2)
        {
            return (bee1.Breed == bee2.Breed &&
                    bee1.IsHasASting == bee2.IsHasASting);
        }
    }
}
